def finding_a_way(matrix: list, s_p: list) -> list:
    '''Функция определения точки выхода из лабиринта'''
    i, j = s_p  # определяем координаты старта
    # определяем каким образом стартовать
    if i == 0:
        i += 1
        direction: str = 'straight'  # направление движения (прямо или реверсивно)
        counter: int = 1             # номер прогона по одной линии
    elif j == 0:
        j += 1
        direction: str = 'straight'
        counter: int = 2             # так как стартуем с боковой грани, то ищем выход только на противоположной грани и вниз
    else:
        j -= 1
        direction: str = 'reverse'
        counter: int = 2
    while True:

        while j <= len(matrix[i])-1 and direction == 'straight' and counter == 1:  # ищем выход по правой стороне
            if j == len(matrix[i])-1 or i == len(matrix)-1:
                return [i, j]
            elif j < len(matrix[i])-1 and matrix[i][j + 1] == '.':
                j += 1
            elif j < len(matrix[i])-1 and matrix[i][j + 1] == '#':
                counter = 2                                                 # уходим на поиск выхода по второму кругу
                direction = 'reverse'                                       # разворачиваем направление двежения
                break

        while j >= 0 and direction == 'reverse' and counter == 2:    # ищем выход по левой стороне
            if j == 0 or i == len(matrix)-1:
                return [i, j]
            elif j > 0 and matrix[i][j - 1] == '.':
                j -= 1
            elif j > 0 and matrix[i][j - 1] == '#':
                counter = 3                                          # уходим на поиск выхода по третьему кругу
                direction = 'straight'                               # разворачиваем направление двежения
                break

        while j <= len(matrix[i])-1 and direction == 'straight' and counter == 3:  # ищем выход вниз
            if j == len(matrix[i])-1 or i == len(matrix)-1:
                return [i, j]
            elif j < len(matrix[i])-1 and matrix[i + 1][j] == '.':
                i += 1
                counter = 1                                  # переходим на новую строку и сбрасываем счетчик
                direction = 'reverse'                        # разворачиваем направление двежения
                break
            elif j < len(matrix[i])-1 and matrix[i + 1][j] == '#' and matrix[i][j + 1] == '.':
                j += 1

        while j >= 0 and direction == 'reverse' and counter == 1:  # ищем выход по левой стороне
            if j == 0 or i == len(matrix)-1:
                return [i, j]
            elif j > 0 and matrix[i][j - 1] == '.':
                j -= 1
            elif j > 0 and matrix[i][j - 1] == '#':
                counter = 2                                         # уходим на поиск выхода по второму кругу
                direction = 'straight'                              # разворачиваем направление двежения
                break

        while j <= len(matrix[i])-1 and direction == 'straight' and counter == 2:  # ищем выход по правой стороне
            if j == 0 or i == len(matrix)-1:
                return [i, j]
            elif j < len(matrix[i])-1 and matrix[i][j + 1] == '.':
                j += 1
            elif j < len(matrix[i])-1 and matrix[i][j + 1] == '#':
                counter = 3                                                # уходим на поиск выхода по третьему кругу
                direction = 'reverse'                                      # разворачиваем направление двежения
                break

        while j >= 0 and direction == 'reverse' and counter == 3:    # ищем выход вниз
            if j == 0 or i == len(matrix)-1:
                return [i, j]
            elif j > 0 and matrix[i + 1][j] == '.':
                i += 1
                counter = 1                                          # переходим на новую строку и сбрасываем счетчик
                direction = 'straight'                               # разворачиваем направление двежения
                break
            elif j > 0 and matrix[i + 1][j] == '#' and matrix[i][j - 1] == '.':
                j -= 1


# Задаем лабиринт
#        0   1   2   3   4   5   6   7   8   9
mtx = [['#','#','#','#','.','#','#','#','#','#'],  # 0
       ['#','.','.','.','.','.','.','.','.','.'],  # 1
       ['#','#','#','#','#','#','.','#','#','#'],  # 2
       ['#','.','.','.','.','.','.','#','#','#'],  # 3
       ['#','.','#','#','#','#','.','#','#','#'],  # 4
       ['#','.','.','.','.','.','.','.','.','#'],  # 5
       ['#','#','.','#','#','#','#','#','#','#'],  # 6
       ['#','#','.','#','#','.','#','#','#','#'],  # 7
       ['#','#','.','.','.','.','.','.','#','#'],  # 8
       ['#','#','#','#','#','.','#','#','#','#']]  # 9

start_point: list = [1, 9]                                      # стартовая точка

print(f'Стартовая точка {start_point}')
print(f'Финишная точка {finding_a_way(mtx, start_point)}')
