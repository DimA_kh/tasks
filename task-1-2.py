def search_1(numbers: str) -> int:
    '''Функция определения пропущенного числа с циклом'''
    my_list: list = list(map(int, numbers.split()))
    decision_list: list = []
    for i in range(1, len(my_list)):
        decision_list.append(my_list[i] - my_list[i - 1])
    value_max: int = max(decision_list)
    index_max: int = decision_list.index(value_max)
    res = my_list[index_max] + value_max // 2
    return res


def search_2(numbers: str) -> int:
    '''Функция определения пропущенного числа без циклов'''
    def find_diff(elem):
        if numbers_list.index(elem) < len(numbers_list)-1:
            return int(numbers_list[numbers_list.index(elem)+1]) - int(elem)
    numbers_list = numbers.split()
    decision_list: list = list(map(find_diff, numbers_list))[:-1]
    value_max: int = max(decision_list)
    index_max: int = decision_list.index(value_max)
    res = int(numbers_list[index_max]) + value_max // 2
    return res


my_numbers = input('Введите последовательность чисел: ')
print(f'Пропущенное число (метод с циклами): {search_1(my_numbers)}')
print(f'Пропущенное число (метод без циклов): {search_2(my_numbers)}')