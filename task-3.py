from string import ascii_lowercase, ascii_uppercase


def main():
    def cipher(text: str, shift: int) -> str:
        '''Функция шифрования'''
        alp_low: str = ascii_lowercase
        alp_up: str = ascii_uppercase
        result: str = ''
        for i in text:
            if i in alp_up:
                result += alp_up[(alp_up.index(i) + shift) % 26]    # делаем сдвиг на заданное число по заглавным буквам
            elif i in alp_low:
                result += alp_low[(alp_low.index(i) + shift) % 26]  # делаем сдвиг на заданное число по строчным буквам
            else:
                result += i                                         # если i не бувка, то просто оставляем данный символ
        return result

    input_shift: int = int(input('value shift: '))
    with open('task_3.txt', encoding='utf-8') as file:
        all_text = file.read().strip('\n')
        print(f'Without shift {all_text}')
        print(f'With shift {cipher(all_text, input_shift)}')


if __name__ == '__main__':
    main()